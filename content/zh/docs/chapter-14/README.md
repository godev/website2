---
title: "Reverse 工具"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-14"
weight: 1510
toc: true
---

## Reverse 工具

reverse 是一个用于进行数据库反转的工具，这是一个独立的工程，见 gitea.com/xorm/reverse .

## 源码安装

```shell
go get xorm.io/reverse
```

同时你需要安装如下依赖，该工程依赖于CGO，请注意要安装CGO环境。

## 使用方法

参见 [https://gitea.com/xorm/reverse](https://gitea.com/xorm/reverse)
